FROM ubuntu:19.10
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y asterisk
COPY extensions.conf /etc/asterisk/extensions.conf
COPY pjsip.conf /etc/asterisk/pjsip.conf
EXPOSE 5060
EXPOSE 5060/udp

CMD ["asterisk", "-fcvvvvv"]