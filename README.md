# Asterisk Container
Small Asterisk container to play around with. By default, 
it runs the [hello world](https://wiki.asterisk.org/wiki/display/AST/Hello+World)
from the Asterisk docs. 

## Building
Run `docker build . -t sometag` then `docker run sometag`

## Testing it out
I used MicroSIP to test this with the following settings
![MicroSIP settings](microsip.png)